package com.game.test.gametest.Villagers.VillagerJobs;

import com.game.test.gametest.Villagers.JobAction;
import com.game.test.gametest.Villagers.VillagerJobs.Jobs.Hunter;
import com.game.test.gametest.Villagers.VillagerJobs.Jobs.Idle;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bbeitman on 11/29/16.
 */

public abstract class Job implements VillagerJob {

    @SerializedName("type")
    private VillagerJob.JobType type;

    @SerializedName("name")
    private String name;

    @SerializedName("level")
    private int level;

    @SerializedName("exp")
    private float exp;

    @SerializedName("modifier")
    private float modifier;

    // IDLE, HUNTER, STONEGATHERER, LUMBERJACK, BUILDER, SCOUT, SQUIRE
    public VillagerJob VillagerJob(VillagerJob.JobType type, String name, int level, float exp, float modifier, List<JobAction> actions) {
        switch (type) {
            case IDLE:
                this.type = JobType.IDLE;
                this.name = "Idle";
                this.level = 0;
                this.exp = 0;
                return new Idle();
            case HUNTER:
                this.type = JobType.HUNTER;
                this.name = "Hunter";
                this.level = 1;
                this.exp = 0;
                calculateModifier();
                return new Hunter();
            case STONEGATHERER:
                this.type = JobType.STONEGATHERER;
                this.name = "Stone Gatherer";
                this.level = 1;
                this.exp = 0;
                calculateModifier();
                return new Hunter();
            case LUMBERJACK:
                this.type = JobType.LUMBERJACK;
                this.name = "Lumberjack";
                this.level = 1;
                this.exp = 0;
                calculateModifier();
                return new Hunter();
            case BUILDER:
                this.type = JobType.BUILDER;
                this.name = "Builder";
                this.level = 1;
                this.exp = 0;
                calculateModifier();
                return new Hunter();
            case SCOUT:
                this.type = JobType.SCOUT;
                this.name = "Scout";
                this.level = 1;
                this.exp = 0;
                calculateModifier();
                return new Hunter();
            case SQUIRE:
                this.type = JobType.SQUIRE;
                this.name = "Squire";
                this.level = 1;
                this.exp = 0;
                calculateModifier();
                return new Hunter();
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public VillagerJob.JobType getType() {
        return type;
    }

    public void setType(VillagerJob.JobType type) {
        this.type = type;
    }

    public void calculateModifier() {
        this.modifier = (1f/level) * .5f;
    }

    public void incExp() {
        exp += modifier;
    }

    public int getExp() {
        return Math.round(exp);
    }

    public float getTrueExp() {
        return exp;
    }

    public void setExp(float exp) {
        this.exp = exp;
    }

    public float getModifier() {
        return modifier;
    }

    public void setModifier(float modifier) {
        this.modifier = modifier;
    }
}
