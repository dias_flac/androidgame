package com.game.test.gametest.Villagers.VillagerJobs;

import com.game.test.gametest.Villagers.JobAction;

import java.util.List;

public interface VillagerJob {

    public enum JobType {
        IDLE, HUNTER, STONEGATHERER, LUMBERJACK, BUILDER, SCOUT, SQUIRE
    }

    public VillagerJob VillagerJob();
    public String getName();
    public void setName(String name);
    public int getLevel();
    public void setLevel(int level);
    public JobType getType();
    public void setType(JobType type);
    public void calculateModifier();
    public void incExp();
    public int getExp();
    public float getTrueExp();
    public void setExp(float exp);
    public float getModifier();
    public void setModifier(float modifier);
    public boolean hasActions();
    public List<JobAction> getActions();
    public void setActions(List<JobAction> actions);
}
