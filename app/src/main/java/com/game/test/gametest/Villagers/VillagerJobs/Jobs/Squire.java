package com.game.test.gametest.Villagers.VillagerJobs.Jobs;

import com.game.test.gametest.Villagers.JobAction;
import com.game.test.gametest.Villagers.VillagerJobs.Job;
import com.game.test.gametest.Villagers.VillagerJobs.VillagerJob;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bbeitman on 11/29/16.
 */

public class Squire extends Job {
    private String TAG = "SquireJobObject";

    @SerializedName("actions")
    private List<JobAction> actions;

    public Squire() {
        super.setType(JobType.SQUIRE);
        super.setName("Squire");
        super.setLevel(1);
        super.setExp(0);
        super.calculateModifier();
        this.actions = new ArrayList<>();
    }

    public Squire(String name, int level, float exp, List<JobAction> actions) {
        super.setType(JobType.SQUIRE);
        super.setName(name);
        super.setLevel(level);
        super.setExp(exp);
        super.calculateModifier();
        this.actions = actions;
    }

    public boolean hasActions() {
        return !this.actions.isEmpty();
    }

    public List<JobAction> getActions() {
        return actions;
    }

    public void setActions(List<JobAction> actions) {
        this.actions = actions;
    }

    public VillagerJob VillagerJob() {
        return this;
    }
}
